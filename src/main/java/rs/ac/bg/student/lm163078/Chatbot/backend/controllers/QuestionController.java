package rs.ac.bg.student.lm163078.Chatbot.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel.ERModel;
import rs.ac.bg.student.lm163078.Chatbot.backend.exceptions.QuestionNotFoundException;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.Question;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionRepository;

import java.util.Iterator;
import java.util.Optional;

@RestController
@RequestMapping("/question")
public class QuestionController {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionController(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @GetMapping("/get")
    public Question getQuestion() {

        return questionRepository
                            .findAll()
                            .iterator()
                            .next();
    }

    @GetMapping("{id}/get")
    public Question getSpecificQuestion(@PathVariable int id) throws QuestionNotFoundException {
        Optional<Question> question = questionRepository.findById(id);

        if (!question.isPresent()) {
            throw new QuestionNotFoundException();
        }

        return question.get();
    }

    @PostMapping("save")
    public int saveQuestion(@RequestBody Question question) {
        questionRepository.save(question);

        return question.getId();
    }

}
