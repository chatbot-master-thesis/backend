package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class RelationshipCardinality {
    static Logger logger = LoggerFactory.getLogger(RelationshipCardinality.class);

    private RelationshipCardinalityEnum minOccurrence;
    private RelationshipCardinalityEnum maxOccurrence;

    public RelationshipCardinality() {
    }

    public RelationshipCardinality(String cardinality) {
        String[] cardinalityExploded = cardinality.split("\\.\\.");

        minOccurrence = getCardinality(cardinalityExploded[0]);
        maxOccurrence = getCardinality(cardinalityExploded[1]);
    }

    private RelationshipCardinalityEnum getCardinality(String cardinality) {
        switch (cardinality) {
            case "0":
                return RelationshipCardinalityEnum.ZERO;
            case "1":
                return RelationshipCardinalityEnum.ONE;
            case "n":
            case "N":
                return RelationshipCardinalityEnum.MANY;
            default:
                logger.error(String.format("Unknown relationship cardinality: %s", cardinality));
                return null;

        }
    }
}
