package rs.ac.bg.student.lm163078.Chatbot.backend.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import rs.ac.bg.student.lm163078.Chatbot.backend.entities.WitResponse;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionHelp;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionHelpRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.services.NLPService;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WitNLPService implements NLPService {

    private RestTemplate restTemplate;
    private QuestionHelpRepository questionHelpRepository;

    private Logger logger = LoggerFactory.getLogger(WitNLPService.class);

    @Value("${nlp.wit.url}")
    private String witUrl;
    @Value("${nlp.wit.token}")
    private String witToken;

    @Autowired
    public WitNLPService(RestTemplate restTemplate, QuestionHelpRepository questionHelpRepository) {
        this.restTemplate = restTemplate;
        this.questionHelpRepository = questionHelpRepository;
    }

    @Override
    //TODO: add check for unsuccessful call
    public QuestionHelp computeUserRequest(String userRequest) {
        HttpEntity<String> headers = getHeaders();
        String uri = getUri(userRequest);

        WitResponse witResponse = restTemplate.exchange(uri, HttpMethod.GET, headers, WitResponse.class).getBody();

        Map<String, String> witEntities = witResponse.getEntities().stream()
                                            .collect( Collectors.toMap(
                                                    WitResponse.WitEntity::getName,
                                                    WitResponse.WitEntity::getValue) );

        logger.info(witEntities.toString());

        return questionHelpRepository.findBySubjectAndObjectAndIntentIgnoreCase(
                                        witEntities.get("subject"),
                                        witEntities.get("object"),
                                        witEntities.get("intent"));

    }

    //TODO: see how to handle exception, maybe not handle it in this class
    private String getUri(String userRequest) {
        try {
            URI witUri = new URI(witUrl);
            return UriComponentsBuilder.fromUri(witUri)
                    .queryParam("v", "20190220")
                    .queryParam("q", userRequest)
                    .build()
                    .toUri()
                    .toString();

        } catch (URISyntaxException e) {
            logger.error("Wit URI is not properly formatted");
        }

        return null;
    }

    private HttpEntity<String> getHeaders() {
        HttpHeaders headers = new HttpHeaders();

        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(witToken);

        return new HttpEntity<>("parameters", headers);
    }
}
