package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import lombok.Data;

import javax.persistence.*;

@Data
@javax.persistence.Entity
public class Attribute {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
}
