package rs.ac.bg.student.lm163078.Chatbot.backend.models.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import rs.ac.bg.student.lm163078.Chatbot.backend.entities.WitResponse;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class WitResponseDeserializer extends StdDeserializer<WitResponse> {
    public WitResponseDeserializer() {
        this(null);
    }

    public WitResponseDeserializer(Class<?> clazz) {
        super(clazz);
    }

    @Override
    public WitResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);

        Iterator<Map.Entry<String, JsonNode>> entities = jsonNode.get("entities").fields();
        WitResponse witResponse = new WitResponse();

        while(entities.hasNext()) {
            Map.Entry<String, JsonNode> entity = entities.next();

            WitResponse.WitEntity witEntity = new WitResponse.WitEntity();

            witEntity.setName(entity.getKey());

            JsonNode witEntityJsonNodes = entity.getValue();

            for (JsonNode witEntityJsonNode: witEntityJsonNodes) {

                double confidence = witEntityJsonNode.get("confidence").asDouble();
                witEntity.setConfidence(confidence);

                String value = witEntityJsonNode.get("value").asText();
                witEntity.setValue(value);

                witResponse.getEntities().add(witEntity);
            }
        }

        return witResponse;
    }
}
