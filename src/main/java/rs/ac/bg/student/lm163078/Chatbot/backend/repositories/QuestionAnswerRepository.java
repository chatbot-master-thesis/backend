package rs.ac.bg.student.lm163078.Chatbot.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionAnswer;

public interface QuestionAnswerRepository extends CrudRepository<QuestionAnswer, Integer> {

}
