package rs.ac.bg.student.lm163078.Chatbot.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Question with specified id not found")
public class QuestionNotFoundException extends Exception {
}
