package rs.ac.bg.student.lm163078.Chatbot.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionHelp;

@Repository
public interface QuestionHelpRepository extends CrudRepository<QuestionHelp, Integer> {
    public QuestionHelp findBySubjectAndObjectAndIntentIgnoreCase(String subject, String object, String intent);
}
