package rs.ac.bg.student.lm163078.Chatbot.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.Question;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Integer> {
}
