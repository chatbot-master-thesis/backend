package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

public enum RelationshipCardinalityEnum {
    ZERO,
    ONE,
    MANY
}
