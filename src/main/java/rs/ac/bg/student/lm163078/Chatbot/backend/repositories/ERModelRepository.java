package rs.ac.bg.student.lm163078.Chatbot.backend.repositories;

import org.springframework.data.repository.CrudRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel.ERModel;

public interface ERModelRepository extends CrudRepository<ERModel, Integer> {
}
