package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import lombok.Data;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.List;

@Data
@Entity
public class ERModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ERElement> elements;
}
