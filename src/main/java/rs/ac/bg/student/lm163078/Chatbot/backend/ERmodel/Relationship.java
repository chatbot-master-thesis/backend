package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
public class Relationship extends ERElement {
    private String firstEntity;
    private String secondEntity;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "minOccurrence",
                    column = @Column(name = "first_to_second.min_occurrence")),
            @AttributeOverride(name = "maxOccurrence",
                    column = @Column(name = "first_to_second.max_occurrence"))
    })
    private RelationshipCardinality firstEntityToSecondEntity;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "minOccurrence",
                    column = @Column(name = "second_to_first.min_occurrence")),
            @AttributeOverride(name = "maxOccurrence",
                    column = @Column(name = "second_to_first.max_occurrence"))
    })
    private RelationshipCardinality secondEntityToFirstEntity;
}
