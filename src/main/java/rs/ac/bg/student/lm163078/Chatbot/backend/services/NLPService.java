package rs.ac.bg.student.lm163078.Chatbot.backend.services;

import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionHelp;

public interface NLPService {
    QuestionHelp computeUserRequest(String userRequest);
}
