package rs.ac.bg.student.lm163078.Chatbot.backend.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.student.lm163078.Chatbot.backend.entities.UserQuestion;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionHelp;
import rs.ac.bg.student.lm163078.Chatbot.backend.services.NLPService;

@RestController
@RequestMapping("/question-help")
public class QuestionHelpController {
    private Logger logger = LoggerFactory.getLogger(QuestionHelpController.class);

    private NLPService nlpService;

    @Autowired
    public QuestionHelpController(NLPService nlpService) {
        this.nlpService = nlpService;
    }

    @PostMapping("/{id}/ask")
    public String askHelp(@PathVariable int id, @RequestBody UserQuestion userQuestion) {

        QuestionHelp questionHelp = nlpService.computeUserRequest(userQuestion.getQuestion());

        if(questionHelp != null) {
            logger.info(questionHelp.toString());
        }

        return questionHelp.getAnswer();
    }

}
