package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@javax.persistence.Entity
public class Entity extends ERElement {
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Attribute> attributes;
}
