package rs.ac.bg.student.lm163078.Chatbot.backend.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel.ERModel;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.Question;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionAnswer;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.ERModelRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionAnswerRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionRepository;

// TODO: Connect everything in Question controller, extract logic to services
@RestController
@RequestMapping("/ermodel")
public class QuestionAnswerController {
    private Logger logger = LoggerFactory.getLogger(QuestionAnswerController.class);

    private final ERModelRepository erModelRepository;
    private final QuestionAnswerRepository questionAnswerRepository;
    private final QuestionRepository questionRepository;


    @Autowired
    public QuestionAnswerController(ERModelRepository erModelRepository, QuestionAnswerRepository questionAnswerRepository, QuestionRepository questionRepository) {
        this.erModelRepository = erModelRepository;
        this.questionAnswerRepository = questionAnswerRepository;
        this.questionRepository = questionRepository;
    }

    @PostMapping("/{id}/answer")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void consumeAnswer(@PathVariable int id, @RequestBody ERModel ermodel) {
        logger.debug(String.format("%s", ermodel));

        erModelRepository.save(ermodel);

        // TODO: check if null
        Question question = questionRepository.findById(id).get();

        QuestionAnswer questionAnswer = new QuestionAnswer();
        questionAnswer.setErModel(ermodel);
        questionAnswer.setQuestion(question);

        questionAnswerRepository.save(questionAnswer);
    }
}
