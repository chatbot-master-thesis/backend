package rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, include = PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Entity.class, name = "entity"),
        @JsonSubTypes.Type(value = Relationship.class, name = "relationship")
})
@javax.persistence.Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class ERElement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
}
