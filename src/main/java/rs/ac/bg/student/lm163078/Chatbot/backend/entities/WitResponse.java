package rs.ac.bg.student.lm163078.Chatbot.backend.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.mapper.WitResponseDeserializer;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonDeserialize(using = WitResponseDeserializer.class)
public class WitResponse {

    @Data
    public static class WitEntity {
        private double confidence;
        private String name;
        private String value;
    }

    List<WitEntity> entities = new ArrayList<>();
}
