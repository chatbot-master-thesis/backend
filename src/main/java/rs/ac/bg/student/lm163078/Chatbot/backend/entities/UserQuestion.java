package rs.ac.bg.student.lm163078.Chatbot.backend.entities;

import lombok.Data;

@Data
public class UserQuestion {
    private String question;
}
