package rs.ac.bg.student.lm163078.Chatbot.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.Question;
import rs.ac.bg.student.lm163078.Chatbot.backend.models.QuestionHelp;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionHelpRepository;
import rs.ac.bg.student.lm163078.Chatbot.backend.repositories.QuestionRepository;

@SpringBootApplication
public class ChatbotBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatbotBackendApplication.class, args);
	}


	@Bean
	@Autowired
	public CommandLineRunner loadData(QuestionRepository questionRepository, QuestionHelpRepository questionHelpRepository) {
		return args -> {
			Question initialQuestion = new Question();
			initialQuestion.setText("A university consists of a number of departments. Each department offers several courses. A number of modules make up each course. Students enrol in a particular course and take modules towards the completion of that course. Each module is taught by a lecturer from the appropriate department, and each lecturer tutors a group of students");
			questionRepository.save(initialQuestion);

			QuestionHelp questionHelp1 = new QuestionHelp();
			QuestionHelp questionHelp2 = new QuestionHelp();

			questionHelp1.setSubject("university");
			questionHelp1.setObject("departments");
			questionHelp1.setIntent("have_many");
			questionHelp1.setAnswer("University can have more departments");

			questionHelpRepository.save(questionHelp1);

			questionHelp2.setSubject("department");
			questionHelp2.setObject("courses");
			questionHelp2.setIntent("have_many");
			questionHelp2.setAnswer("Department can have more courses");

			questionHelpRepository.save(questionHelp2);
		};
	}

}