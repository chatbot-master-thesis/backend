package rs.ac.bg.student.lm163078.Chatbot.backend.models;

import rs.ac.bg.student.lm163078.Chatbot.backend.ERmodel.ERModel;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
public class QuestionAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private ERModel erModel;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Question question;
}
